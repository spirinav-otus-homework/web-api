﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;

using ConsoleTableExt;

using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;

using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace ConsoleApp
{
	class Program
	{
		private static HttpClient _httpClient;
		private static HttpResponseMessage _response;
		private static string _jsonString;

		private const string TableMenuText =
			"\nSelect action:\n[1] - get all users\n[2] - get user by id\n[3] - add user\n[4] - exit\n\nYour choice:";

		private const string WrongResultText = "\nSomething went wrong. Result: {0}.";
		private const string NoUsersText = "\nThere are no users in the database.";
		private const string IncorrectInputText = "\nIncorrect input. Try again.";
		private const string EnterIdText = "\nEnter id:";
		private const string EnterNewIdText = "\nEnter id for new user:";
		private const string SuccessfullyAddedText = "\nNew user has been successfully added.";

		static async Task Main()
		{
			_httpClient = new HttpClient {BaseAddress = new Uri(GetBaseUrl())};

			while (true)
			{
				MessageOutput(TableMenuText);
				switch (Console.ReadLine())
				{
					case "1":
						await GetAllUsers();
						break;
					case "2":
						await GetUserById();
						break;
					case "3":
						await AddNewUser();
						break;
					case "4":
						return;
					default:
						ErrorOutput(IncorrectInputText);
						break;
				}
			}
		}

		private static async Task GetAllUsers()
		{
			_response = await _httpClient.GetAsync("api/users/");

			if (_response.StatusCode != HttpStatusCode.OK)
			{
				ErrorOutput(string.Format(WrongResultText, _response.StatusCode));
			}
			else
			{
				_jsonString = await _response.Content.ReadAsStringAsync();

				var users = JsonConvert.DeserializeObject<List<User>>(_jsonString);

				if (users == null || !users.Any())
				{
					ResultOutput(NoUsersText);
					return;
				}

				ResultOutput(users.Select(x => new {x.Id, x.Login, x.FullName, x.Email, x.Phone})
					.OrderBy(x => x.Id).ToList(), "Users");
			}
		}

		private static async Task GetUserById()
		{
			while (true)
			{
				MessageOutput(EnterIdText);

				if (!int.TryParse(Console.ReadLine(), out var id))
				{
					ErrorOutput(IncorrectInputText);
					continue;
				}

				_response = await _httpClient.GetAsync($"api/users/{id}");

				if (_response.StatusCode != HttpStatusCode.OK)
				{
					ErrorOutput(string.Format(WrongResultText, _response.StatusCode));
				}
				else
				{
					_jsonString = await _response.Content.ReadAsStringAsync();

					var users = new List<User> {JsonConvert.DeserializeObject<User>(_jsonString)};

					ResultOutput(users.Select(x => new {x.Id, x.Login, x.FullName, x.Email, x.Phone})
						.OrderBy(x => x.Id).ToList(), $"User {id}");
				}

				return;
			}
		}

		private static async Task AddNewUser()
		{
			while (true)
			{
				MessageOutput(EnterNewIdText);

				if (!int.TryParse(Console.ReadLine(), out var id))
				{
					ErrorOutput(IncorrectInputText);
					continue;
				}

				var newUser = RandomUserGenerator.CreateFaker(id).Generate();

				_response = await _httpClient.PostAsync("api/users/", newUser, new JsonMediaTypeFormatter());

				if (_response.StatusCode != HttpStatusCode.OK)
				{
					ErrorOutput(string.Format(WrongResultText, _response.StatusCode));
				}
				else
				{
					ResultOutput(SuccessfullyAddedText);
				}

				return;
			}
		}

		private static string GetBaseUrl()
		{
			return new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", true)
				.Build()
				.GetSection("BaseUrl")
				.Value;
		}

		private static void Output(string output, ConsoleColor color)
		{
			Console.ForegroundColor = color;
			Console.WriteLine(output);
			Console.ResetColor();
		}

		public static void MessageOutput(string message)
		{
			Output(message, ConsoleColor.DarkYellow);
		}

		public static void ErrorOutput(string error)
		{
			Output(error, ConsoleColor.DarkRed);
		}

		public static void ResultOutput(string result)
		{
			Output(result, ConsoleColor.DarkGreen);
		}

		public static void ResultOutput<T>(List<T> result, string name) where T : class
		{
			Console.WriteLine();
			Console.ForegroundColor = ConsoleColor.DarkGreen;
			ConsoleTableBuilder
				.From(result)
				.WithTitle(name, TextAligntment.Left)
				.WithFormat(ConsoleTableBuilderFormat.MarkDown)
				.ExportAndWriteLine();
			Console.ResetColor();
		}
	}
}