## Взаимодействие между клиентом и сервером
### Цель
Применить на практике полученные знания по работе с REST API.
### Описание программы
Для проекта Otus.Teaching.Concurrency.Import необходимо добавить два внутренних проекта - WebApi и консольный клиент.  
Консольный клиент должен отправлять в WebApi запрос на сохранение случайным образом сгенерированного пользователя.  
Сервер, в свою очередь, при добавлении пользователя должен проверять наличие пользователя в БД и возвращать коды ответов:
* 200, если пользователь добавлен без ошибок;
* 409, если пользователь с таким Id уже существует в базе.  

Также, должен быть метод получения пользователя по идентификатору, а сервер должен отдавать статус-код 200 с информацией о пользователе, если он есть, либо 404, если пользователь не был найден.
### Основное задание
1. Доделать предыдущую работу по проекту https://gitlab.com/devgrav/Otus.Teaching.Concurrency.Import, если она ещё не закончена;
2. Создать проект WebApi с методом GET /users/{id};
3. Создать проект консольного приложения принимающего с консоли ID, запрашивающего и отображающего данные по пользователю;
4. Добавить поддержку метода POST /users/;
5. Добавить генерацию случайного пользователя в консольном приложении и его отправку на сервер для сохранения.
### Критерии оценки
* 1 балл: Сделан пункт №1;
* 2 балл: Сделан пункт №2;
* 3 балл: Сделан пункт №3;
* 1 балл: Сделан пункт №4;
* 1 балл: Сделан пункт №5;
* 1 балл: Сделано 2 CodeReview;
* 1 балл: CodeStyle, грамотная архитектура, всё замечания проверяющего исправлены.
### Условие сдачи
Минимальный проходной балл: 8 баллов.
