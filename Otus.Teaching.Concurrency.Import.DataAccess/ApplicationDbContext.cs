﻿using Microsoft.EntityFrameworkCore;

using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
	public sealed class ApplicationDbContext : DbContext
	{
		public DbSet<User> Users { get; set; }

		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
		{
			Database.EnsureCreated();
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<User>().ToTable("users").HasKey(u => u.Id);
			modelBuilder.Entity<User>().Property(u => u.Id).HasColumnName("id").IsRequired();
			modelBuilder.Entity<User>().Property(u => u.Login).HasColumnName("login").IsRequired().HasMaxLength(100);
			modelBuilder.Entity<User>().Property(u => u.Password).HasColumnName("password").IsRequired().HasMaxLength(25);
			modelBuilder.Entity<User>().Property(u => u.FullName).HasColumnName("full_name").IsRequired().HasMaxLength(100);
			modelBuilder.Entity<User>().Property(u => u.Email).HasColumnName("email").IsRequired().HasMaxLength(100);
			modelBuilder.Entity<User>().Property(u => u.Phone).HasColumnName("phone").IsRequired().HasMaxLength(15);
		}
	}
}