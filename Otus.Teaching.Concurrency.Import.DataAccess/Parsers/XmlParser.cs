﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        private readonly string _file;

        public XmlParser(string file)
        {
            _file = file;
        }

        public List<Customer> Parse()
        {
            var serializer = new XmlSerializer(typeof(CustomersList));
            using var reader = new FileStream(_file, FileMode.Open);
            var customerList = (CustomersList) serializer.Deserialize(reader);

            return customerList?.Customers;
        }
    }
}