﻿namespace Otus.Teaching.Concurrency.Import.Core.Settings
{
	public interface IGeneratorSetting
	{
		public bool RunInNewProcess { get; set; }

		public int CountGenerateObject { get; set; }
	}
}