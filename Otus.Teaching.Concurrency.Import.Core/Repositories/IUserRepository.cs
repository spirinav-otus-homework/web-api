﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
	public interface IUserRepository
	{
		Task<IEnumerable<User>> GetAllAsync();

		Task<User> GetByIdAsync(int id);

		Task<bool> IsExistAsync(int id);

		Task AddAsync(User user);

		Task<int> SaveChangesAsync();
	}
}