﻿using System;
using System.IO;

using Otus.Teaching.Concurrency.Import.Core.Settings;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
	class Program
	{
		private static string _dataFileName;
		private static IFileSetting _fileSetting;
		private static IGeneratorSetting _generatorSetting;

		static void Main(string[] args)
		{
			if (!TryValidateAndParseArgs(args))
				return;

			var fileType = _fileSetting.FileType.ToString().ToLower();
			Console.WriteLine($"Generating {fileType} data...");
			var generator = new GeneratorFactory(_fileSetting, _generatorSetting).GetGenerator();
			generator.Generate();
			Console.WriteLine($"Generated {fileType} data in {_dataFileName}...");
		}

		private static bool TryValidateAndParseArgs(string[] args)
		{
			if (args is null or {Length: 0})
			{
				Console.WriteLine("Data file name without extension is required");
				return false;
			}

			if (args.Length < 2 || !Enum.TryParse<FileType>(args[1], true, out var dataFileType))
			{
				Console.WriteLine("File type is not set");
				return false;
			}

			if (args.Length < 3 || !int.TryParse(args[2], out var dataCount))
			{
				Console.WriteLine("Data count must be integer");
				return false;
			}

			_fileSetting = new FileSetting {FileType = dataFileType, FileName = args[0]};
			_generatorSetting = new GeneratorSetting {CountGenerateObject = dataCount};
			_dataFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
				_fileSetting.FileName + "." + _fileSetting.FileType.ToString().ToLower());

			return true;
		}
	}

	public class FileSetting : IFileSetting
	{
		public FileType FileType { get; set; }

		public string FileName { get; set; }
	}

	public class GeneratorSetting : IGeneratorSetting
	{
		public bool RunInNewProcess { get; set; }

		public int CountGenerateObject { get; set; }
	}
}