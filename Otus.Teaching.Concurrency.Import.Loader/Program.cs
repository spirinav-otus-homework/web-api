﻿using System;
using System.Diagnostics;
using System.IO;

using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Settings;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace Otus.Teaching.Concurrency.Import.Loader
{
	class Program
	{
		private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");

		static void Main(string[] args)
		{
			if (args != null && args.Length == 1)
			{
				_dataFilePath = args[0];
			}

			Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

			GenerateCustomersDataFile();

			var customers = new XmlParser(_dataFilePath).Parse();

			var loader = new FakeDataLoader(customers, new LoaderSetting{ThreadCount = 10});

			loader.LoadData();
		}

		static void GenerateCustomersDataFile()
		{
			var xmlGenerator = new XmlGenerator(_dataFilePath, 1000);
			xmlGenerator.Generate();
		}
	}

	public class LoaderSetting : ILoaderSetting
	{
		public bool UseDataBase { get; set; }
		public bool UseThreadPool { get; set; }
		public int ThreadCount { get; set; }
	}
}