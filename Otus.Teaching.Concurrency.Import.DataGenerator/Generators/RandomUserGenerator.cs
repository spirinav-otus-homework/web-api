﻿using Bogus;

using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
	public class RandomUserGenerator
	{
		public static Faker<User> CreateFaker(int id)
		{
			var userFaker = new Faker<User>()
				.CustomInstantiator(f => new User()
				{
					Id = id++
				})
				.RuleFor(x => x.Login, y => y.Person.UserName)
				.RuleFor(x => x.Password, y => y.Random.String(10, 10))
				.RuleFor(u => u.FullName, (f, u) => f.Name.FullName())
				.RuleFor(x => x.Email, y => y.Internet.Email())
				.RuleFor(u => u.Phone, (f, u) => f.Phone.PhoneNumber("1-###-###-####"));

			return userFaker;
		}
	}
}